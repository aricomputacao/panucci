import 'package:flutter/material.dart';
import 'package:panucci_ristorante/cardapio.dart';
import 'package:panucci_ristorante/components/highlight_item.dart';
import 'package:panucci_ristorante/components/util/main_sliver_to_boxadapter.dart';

class Highlights extends StatelessWidget {
  const Highlights({super.key});

  final List items = destaques;

  @override
  Widget build(BuildContext context) {
    bool landscape =
        MediaQuery.of(context).orientation == Orientation.landscape;

    return Padding(
      padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
      child: CustomScrollView(
        slivers: <Widget>[
          const MainSliverToBoxAdapter(
              title: "Destaques",
              textAlign: TextAlign.center,
              textStyle: TextStyle(fontFamily: "Caveat", fontSize: 32)),
          landscape
              ? SliverGrid(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return HighlightItem(
                          imageURI: items[index]["image"],
                          itemTitle: items[index]["name"],
                          itemPrice: items[index]["price"],
                          itemDescription: items[index]["description"]);
                    },
                    childCount: items.length,
                  ),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 8,
                  ),
                )
              : SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return HighlightItem(
                          imageURI: items[index]["image"],
                          itemTitle: items[index]["name"],
                          itemPrice: items[index]["price"],
                          itemDescription: items[index]["description"]);
                    },
                    childCount: items.length,
                  ),
                )
        ],
      ),
    );
  }
}
