import 'package:flutter/material.dart';
import 'package:panucci_ristorante/cardapio.dart';
import 'package:panucci_ristorante/components/food_item.dart';
import 'package:panucci_ristorante/components/util/main_sliver_to_boxadapter.dart';

class FoodMenu extends StatelessWidget {
  const FoodMenu({super.key});

  final List items = comidas;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
      child: CustomScrollView(
        slivers: <Widget>[
          const MainSliverToBoxAdapter(
              title: "Menu",
              textAlign: TextAlign.center,
              textStyle: TextStyle(fontFamily: "Caveat", fontSize: 32)),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                return FoodItem(
                    itemTitle: items[index]["name"],
                    itemPrice: items[index]["price"],
                    imageURI: items[index]["image"]);
              },
              childCount: items.length,
            ),
          ),
        ],
      ),
    );
  }
}
