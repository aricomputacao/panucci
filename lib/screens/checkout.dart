import 'package:flutter/material.dart';
import 'package:panucci_ristorante/cardapio.dart';
import 'package:panucci_ristorante/components/main_drawer.dart';
import 'package:panucci_ristorante/components/order_item.dart';
import 'package:panucci_ristorante/components/payment_method.dart';
import 'package:panucci_ristorante/components/payment_total.dart';
import 'package:panucci_ristorante/components/util/main_appbar.dart';
import 'package:panucci_ristorante/components/util/main_sliver_to_boxadapter.dart';

class Checkout extends StatelessWidget {
  const Checkout({super.key});

  final List items = pedido;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MainAppBar(),
      drawer: const MainDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: CustomScrollView(
          slivers: <Widget>[
            const MainSliverToBoxAdapter(
              title: "Pedidos",
              textAlign: TextAlign.start,
              textStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
            ),
            SliverList(
                delegate: SliverChildBuilderDelegate(
              (context, index) {
                return OrderItem(
                    imageURI: items[index]["image"],
                    itemTitle: items[index]["name"],
                    itemPrice: items[index]["price"]);
              },
              childCount: items.length,
            )),
            const MainSliverToBoxAdapter(
              title: "Pagamento",
              textAlign: TextAlign.start,
              textStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
            ),
            SliverToBoxAdapter(
              child: PaymentMethod(),
            ),
            const MainSliverToBoxAdapter(
              title: "Confirmar",
              textAlign: TextAlign.start,
              textStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
            ),
            SliverToBoxAdapter(child: PaymentTotal(),),
          ],
        ),
      ),
    );
  }
}
