import 'package:flutter/material.dart';

class MainSliverToBoxAdapter extends StatelessWidget {
  final String title;
  final TextAlign textAlign;
  final TextStyle textStyle;

  const MainSliverToBoxAdapter(
      {super.key,
      required this.title,
      required this.textAlign,
      required this.textStyle});

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Text(
          title,
          style: textStyle,
          textAlign: textAlign,
        ),
      ),
    );
  }
}
