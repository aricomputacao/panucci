import 'package:flutter/material.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  const MainAppBar({Key? key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text("Ristorante Panucci"),
      backgroundColor: Theme.of(context).colorScheme.surfaceVariant,
      actions: const <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Icon(
            Icons.account_circle,
            size: 32,
          ),
        )
      ],
      centerTitle: true,
    );
  }
}
